//adding components
import { useState } from "react";
import Library from "./components/Library";
import Player from "./components/Player";
import Nav from "./components/Nav";
import Song from "./components/Song";
import data from "./data";

import './styling/app.scss';

function App() {
  //useState (song) -> gets the data() info
  const [songs, setSongs] = useState(data());
  //new State to call the currentSong
  const [currentSong, setCurrentSong] = useState(songs[0]);
  //new State to check if a song is playing or not
  const [isPlaying, setIsPlaying] = useState(false)
  const [libraryStatus, setLibraryStatus] = useState(false)

  return (
    <div className={`App ${libraryStatus ? 'library-active' : ""}`}>
      <Nav libraryStatus = {libraryStatus} setLibraryStatus={setLibraryStatus}/>
      <Song currentSong = {currentSong} />
      <Player isPlaying = {isPlaying} setIsPlaying = {setIsPlaying} currentSong = {currentSong} />
      <Library setCurrentSong={setCurrentSong} songs={songs} libraryStatus={libraryStatus} />
    </div>
  );
};

export default App;
