import { useState, useRef } from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlayCircle, faForward, faBackward, faPause} from '@fortawesome/free-solid-svg-icons';

const Player = ({currentSong, isPlaying, setIsPlaying}) =>  {

    const audioRef = useRef(null);

    //calling a State that plays and pauses the song
    const  playSongHandler = () => {
        //if isPlaying -> pause then State
        if(isPlaying) {
            audioRef.current.pause();
            setIsPlaying(!isPlaying)
        //else play and stitch the State
        } else {
            audioRef.current.play();
            setIsPlaying(!isPlaying)
        }
    }

    const [songInfo, setSongInfo] = useState({
        currentTime: null,
        duration: null
    });

    //timeUpdateHandler function
    const timeUpdateHandler = (e) => {
        const currentTime = e.target.currentTime;
        const duration = e.target.duration;

        setSongInfo({
            ...songInfo,
            currentTime: currentTime,
            duration: duration
        });
    }

    // dragHandler function
    const dragHandler = (e) => {
        audioRef.current.currentTime = e.target.value;
        setSongInfo ({...songInfo, currentTime: e.target.value});
    }

    const autoPlayHandler = () => {
        if(isPlaying)
        audioRef.current.play();
    }

    const backHandler = (time) => {
        audioRef.current.currentTime = parseInt(songInfo.currentTime) - 10;
        setSongInfo ({...songInfo, currentTime: parseInt(songInfo.currentTime) - 10});
    }

    const forwardHandler = (time) => {
        audioRef.current.currentTime = parseInt(songInfo.currentTime) + 10;
        setSongInfo ({...songInfo, currentTime: parseInt(songInfo.currentTime) + 10});

    }
        

    const getTime = (time) => {
        return (
            Math.floor(time/60) + ":" + ("0" + Math.floor(time % 60)).slice(-2)
        );
    };


    return (
       <div className="player">
            <div className="time-control">
                <p>{getTime(songInfo.currentTime)}</p>
                <input onChange={dragHandler} min={0} max={songInfo.duration} value={songInfo.currentTime} type="range" />
                <p>{getTime(songInfo.duration)}</p>
            </div>
            <div className="play-control">
                <FontAwesomeIcon className="Back" onClick={backHandler} icon={faBackward} size="2x"></FontAwesomeIcon>
                <FontAwesomeIcon className="play" onClick={playSongHandler} icon={isPlaying ? faPause : faPlayCircle} size="2x"></FontAwesomeIcon>
                <FontAwesomeIcon className="Forward" onClick={forwardHandler} icon={faForward} size="2x"></FontAwesomeIcon>
            </div>
            <audio 
                onTimeUpdate={timeUpdateHandler}
                onLoadedData={autoPlayHandler}
                onLoadedMetadata={timeUpdateHandler} 
                ref={audioRef} 
                src={currentSong.audio}>
                </audio>
       </div> 

    );
};

export default Player;